FROM python:3

# This is needed because of an error when using openCV
#   ImportError: libGL.so.1: cannot open shared object file: No such file or directory
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y

WORKDIR /app

COPY requirements.txt .
COPY static/ static/
COPY *.py ./

RUN pip install -r requirements.txt

EXPOSE 8080
CMD ["python", "app.py"]
# Autocrop API

This project is based on [Autocrop](https://github.com/leblancfg/autocrop).

Autocrop is a application which detects and crops the largest face of a given picture. This project makes autocrop
available as a single service.

## Defaults

- The application is available on port *8080*
- When no height/width is given the face will be cropped as *500 x 500 px*

## Usage

### Start the application

You can run the project locally or use the docker-image. For local usage you have to clone the project beforehand.

```sh
# Python
pip install -r requirements.txt
python3 app.py

# Docker
docker run -d -p 8080:8080 mbedded/autocrop-service:1.0.0
```

### Interaction 

When you access http://localhost:8080/ you'll get a sample website to see if the service is running.

**Without preview**

POST `http://localhost:8080/crop?width=500&height=250`

Request
```json
{
  "image": "IMAGE_AS_BASE64"
}
```

Response
```json
{
  "croppedImage": "CROPPED_IMAGE_AS_BASE64"
}
```

Sample by a REST-Client:
![Cropping result in a REST-Client](docs/autocrop_sample.png)

Sample with cURL:
```shell
curl --request POST \
     --url 'http://localhost:8080/crop?width=500&height=250' \
     --header 'content-type: application/json' \
     --data '{ "image":"YOUR_IMAGE_BASE64_ENCODED" }'
```


**With preview**

POST `http://localhost:8080/crop?width=500&height=250&preview=true`

Pay attention to the *preview* parameter in the URL.

The Request body remains the same. The response contains the raw-bytes
of an PNG image (**not** base64 encoded).

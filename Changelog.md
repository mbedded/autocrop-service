# Changelog

## 1.0
- Initial version
- Update Readme and additional information (#2)
- Publish Dockerfile on Dockerhub(#1)

import io
import os
import uuid
import base64

from waitress import serve
from autocrop.autocrop import ImageReadError
from flask import Flask, jsonify, request, send_file
from autocrop import Cropper
from PIL import Image

from ParsedRequest import ParsedRequest

DEFAULT_SIZE = 500
app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('index.html')


def parse_request(request):
    req = ParsedRequest()

    try:
        data = request.json

        # Check for image
        if 'image' in data:
            req.image = data['image']
        else:
            req.is_valid = False
            return req

        req.is_valid = True

    except:
        req.is_valid = False

    width = request.args.get('width', type=int)
    height = request.args.get('height', type=int)

    if width is None:
        width = DEFAULT_SIZE

    if height is None:
        height = DEFAULT_SIZE

    req.width = width
    req.height = height

    return req


@app.route('/crop', methods=['POST'])
def parse_image():
    parsed = parse_request(request)

    if not parsed.is_valid:
        return jsonify(message="Unable to parse json. Ensure 'image' contains the image file as base64"), 400

    show_preview = request.args.get('preview', type=bool)
    input_filename = f"tmp_{uuid.uuid4()}.png"

    with open(input_filename, "wb") as f:
        bytes = base64.b64decode(parsed.image)
        f.write(bytes)

    cropper = Cropper(width=parsed.width, height=parsed.height)

    image = None
    try:
        image = cropper.crop(input_filename)
    except ImageReadError:
        return jsonify(message="Unable to parse image. Ensure the request body is a valid image file"), 400
    finally:
        os.remove(input_filename)

    if isinstance(image, type(None)):
        return jsonify(message="No face detected"), 400

    else:
        buffer = io.BytesIO()
        img_out = Image.fromarray(image)
        img_out.save(buffer, format='PNG')

        if show_preview:
            buffer.seek(0)
            return send_file(buffer, 'image/png')

        else:
            base64_str = base64.b64encode(buffer.getvalue()).decode('UTF-8')
            return jsonify(croppedImage=base64_str)


if __name__ == '__main__':
    # flask
    # app.run()

    serve(app, host='0.0.0.0', port=8080)

#!/bin/sh
DOCKER_TAG=mbedded/autocrop-service:$1
docker build -t $DOCKER_TAG .

docker login
docker push $DOCKER_TAG
docker logout
